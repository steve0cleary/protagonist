import json
import boto3
import requests
import uuid
import datetime
from boto3.dynamodb.conditions import Key
import os

# Initialize the AWS SDK and DynamoDB table
dynamodb = boto3.resource('dynamodb')

def extract_flight_details(item):
    flyFrom = item.get('flyFrom', '')
    flyTo = item.get('flyTo', '')
    duration = item.get('duration', '')
    price = item.get('price', '')

    return {
        'flyFrom': flyFrom,
        'flyTo': flyTo,
        'duration': duration,
        'price': price
    }

def query_flight_data(table):
    try:
        response = table.scan()
        items = response['Items']
        flight_details = [extract_flight_details(item) for item in items]

        return {
            'statusCode': 200,
            'body': {
                'message': 'Flight data retrieved successfully',
                'data': flight_details
            }
        }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': {
                'message': 'An error occurred while retrieving flight data',
                'error': str(e)
            }
        }

def lambda_handler(event, context):
    # Retrieve the DynamoDB table name from the event object
    dynamodb_table = os.environ.get('DYNAMODB_CUSTOMER_TABLE')

    # Create a DynamoDB table object
    table = dynamodb.Table(dynamodb_table)

    # Check if this is a request to query flight data
    if event.get('action') == 'query_flight_data':
        return query_flight_data(table)

    api_key = 'glpat-hgsbsPvrsssuCHZjdQ2J'
    endpoint = 'https://tequila-api.kiwi.com/v2/search'

    
    current_timestamp = datetime.datetime.now().isoformat()

    params = {
        'fly_from': event.get('fly_from', 'ATW'),
        'fly_to': event.get('fly_to', 'AUS'),
        'date_from': event.get('date_from', '2023-11-16'),
        'date_to': event.get('date_to', '2024-01-01'),
        'adults': int(event.get('adults', 1)),
        'currency': event.get('currency', 'USD'),
        'partner_market': event.get('partner_market', 'us'),
        'sort': 'price',
        'limit': 5
    }

    headers = {
        'apikey': api_key,
    }

    try:
        # GET request to the Kiwi.com API
        response = requests.get(endpoint, params=params, headers=headers)

        # Check if the request was successful
        if response.status_code == 200:
            # Extract flight details from the API response
            data = response.json()
            if "data" in data:
                flight_data = data["data"]
                flight_details = [extract_flight_details(item) for item in flight_data]
                if flight_details:
                    # Create formatted flight details list
                    formatted_flight_details = []
                    for index, flight_detail in enumerate(flight_details):
                        search_id = str(uuid.uuid4())
                        table.put_item(
                            Item={
                                'search_id': search_id,
                                'fly_from': flight_detail["flyFrom"],
                                'fly_to': flight_detail["flyTo"],
                                'duration': flight_detail["duration"],
                                'price': flight_detail["price"],
                                'timestamp': current_timestamp,
                                'item_index': index  # Add an index to distinguish between items
                            }
                        )
                        # Append to formatted_flight_details
                        formatted_flight_details.append({
                            'search_id': search_id,
                            'fly_from': flight_detail["flyFrom"],
                            'fly_to': flight_detail["flyTo"],
                            'duration': flight_detail["duration"],
                            'price': flight_detail["price"],
                            'timestamp': current_timestamp,
                            'item_index': index
                        })

                    
                    return {
                        'statusCode': 200,
                        'body': json.dumps({
                            'message': 'Flight data retrieved and stored in DynamoDB successfully',
                            'formatted_flight_details': formatted_flight_details
                        })
                    }
                else:
                    return {
                        'statusCode': 500,
                        'body': json.dumps({
                            'message': 'Failed to extract flight details from the API response',
                            'formatted_flight_details': []
                        })
                    }
            else:
                return {
                    'statusCode': 500,
                    'body': json.dumps({
                        'message': 'No "data" field in API response',
                        'formatted_flight_details': []
                    })
                }
        else:
            return {
                'statusCode': response.status_code,
                'body': json.dumps({
                    'message': f"Request failed with status code: {response.status_code}",
                    'formatted_flight_details': []
                })
            }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps({
                'message': 'An error occurred',
                'error': str(e),
                'formatted_flight_details': []
            })
        }

